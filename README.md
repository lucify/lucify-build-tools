This repo has moved to [GitHub](https://github.com/lucified/lucify-build-tools).
Update your git remote with:

```bash
git remote set-url origin git@github.com:lucified/lucify-build-tools.git
```
